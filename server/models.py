from django.db import models


class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    description = models.TextField(null=True)
    tax_percent = models.IntegerField(default=0)


# Create your models here.
class Menu(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.TextField(null=True)
    active = models.BooleanField(default=False)
    schedule = models.JSONField()
    active_from = models.DateTimeField()
    active_to = models.DateTimeField(null=True)


class MenuItem(models.Model):
    name = models.CharField(max_length=100)
    menu = models.ForeignKey(Menu, on_delete=models.SET_NULL, null=True)
    description = models.TextField()
    price = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    addons = models.ManyToManyField('Addons')
    subtracts = models.ManyToManyField('Subtracts')
    picture = models.CharField(max_length=1024, null=True)
    section = models.ForeignKey('Section', on_delete=models.SET_NULL, null=True)


class Section(models.Model):
    name = models.CharField(max_length=50)


class Addons(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    multi_select = models.BooleanField(default=True)


class Subtracts(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(default=0, max_digits=6, decimal_places=2)


class Order(models.Model):

    class OrderStatus(models.TextChoices):
        NEW = 'New'
        IN_PROCESS = 'In Process'
        OUT_FOR_DELIVERY = 'Out for Delivery'
        COMPLETE = 'Complete'
        CANCELLED = 'Cancelled'

    pickup = models.BooleanField(default=False)
    client = models.ForeignKey('Client', on_delete=models.RESTRICT)
    order_items = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(
        max_length=20,
        choices=OrderStatus.choices,
        default=OrderStatus.NEW
    )
    total_price = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    picked_up = models.DateTimeField(null=True)
    delivered = models.DateTimeField(null=True)


class Client(models.Model):
    firstName = models.CharField(max_length=100, null=True)
    lastName = models.CharField(max_length=100, null=True)
    address = models.TextField(null=True)
    phone = models.CharField(max_length=12, null=True)
    deleted = models.DateTimeField(null=True)


class Driver(models.Model):
    firstName = models.CharField(max_length=100, null=True)
    lastName = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=12, null=True)
    deleted = models.DateTimeField(null=True)


class Delivery(models.Model):

    class DeliveryStatus(models.TextChoices):
        NEW = 'New'
        CLAIMED = 'Claimed'
        OUT_FOR_DELIVERY = 'Out for Delivery'
        COMPLETE = 'Delivered'
        CANCELLED = 'Cancelled'

    order = models.ForeignKey(Order, on_delete=models.RESTRICT)
    driver = models.ForeignKey('Driver', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    driver_fee = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    driver_tip = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    status = models.CharField(
        max_length=20,
        choices=DeliveryStatus.choices,
        default=DeliveryStatus.NEW
    )
