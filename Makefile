

shell:
	python manage.py shell

serve:
	python manage.py runserver


schema:
	python manage.py makemigrations


migrate:
	python manage.py migrate




.PHONY: shell serve schema migrate
